﻿namespace EPP3_Tholdi.Forms
{
    partial class DetailDeclaration
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.radioButtonUrgenceNon = new System.Windows.Forms.RadioButton();
            this.radioButtonUrgenceOui = new System.Windows.Forms.RadioButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.radioButtonTraiterNon = new System.Windows.Forms.RadioButton();
            this.radioButtonTraiterOui = new System.Windows.Forms.RadioButton();
            this.label4 = new System.Windows.Forms.Label();
            this.comboBoxProbleme = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxDescription = new System.Windows.Forms.TextBox();
            this.buttonRetour = new System.Windows.Forms.Button();
            this.buttonValider = new System.Windows.Forms.Button();
            this.buttonSupprimer = new System.Windows.Forms.Button();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.radioButtonUrgenceNon);
            this.groupBox2.Controls.Add(this.radioButtonUrgenceOui);
            this.groupBox2.Location = new System.Drawing.Point(594, 82);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(112, 100);
            this.groupBox2.TabIndex = 8;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Situation urgente";
            // 
            // radioButtonUrgenceNon
            // 
            this.radioButtonUrgenceNon.AutoSize = true;
            this.radioButtonUrgenceNon.Location = new System.Drawing.Point(13, 63);
            this.radioButtonUrgenceNon.Name = "radioButtonUrgenceNon";
            this.radioButtonUrgenceNon.Size = new System.Drawing.Size(45, 17);
            this.radioButtonUrgenceNon.TabIndex = 1;
            this.radioButtonUrgenceNon.TabStop = true;
            this.radioButtonUrgenceNon.Text = "Non";
            this.radioButtonUrgenceNon.UseVisualStyleBackColor = true;
            // 
            // radioButtonUrgenceOui
            // 
            this.radioButtonUrgenceOui.AutoSize = true;
            this.radioButtonUrgenceOui.Location = new System.Drawing.Point(13, 27);
            this.radioButtonUrgenceOui.Name = "radioButtonUrgenceOui";
            this.radioButtonUrgenceOui.Size = new System.Drawing.Size(41, 17);
            this.radioButtonUrgenceOui.TabIndex = 0;
            this.radioButtonUrgenceOui.TabStop = true;
            this.radioButtonUrgenceOui.Text = "Oui";
            this.radioButtonUrgenceOui.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.radioButtonTraiterNon);
            this.groupBox1.Controls.Add(this.radioButtonTraiterOui);
            this.groupBox1.Location = new System.Drawing.Point(445, 82);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(104, 100);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Situation traitée";
            // 
            // radioButtonTraiterNon
            // 
            this.radioButtonTraiterNon.AutoSize = true;
            this.radioButtonTraiterNon.Location = new System.Drawing.Point(13, 63);
            this.radioButtonTraiterNon.Name = "radioButtonTraiterNon";
            this.radioButtonTraiterNon.Size = new System.Drawing.Size(45, 17);
            this.radioButtonTraiterNon.TabIndex = 4;
            this.radioButtonTraiterNon.TabStop = true;
            this.radioButtonTraiterNon.Text = "Non";
            this.radioButtonTraiterNon.UseVisualStyleBackColor = true;
            // 
            // radioButtonTraiterOui
            // 
            this.radioButtonTraiterOui.AutoSize = true;
            this.radioButtonTraiterOui.Location = new System.Drawing.Point(13, 27);
            this.radioButtonTraiterOui.Name = "radioButtonTraiterOui";
            this.radioButtonTraiterOui.Size = new System.Drawing.Size(41, 17);
            this.radioButtonTraiterOui.TabIndex = 3;
            this.radioButtonTraiterOui.TabStop = true;
            this.radioButtonTraiterOui.Text = "Oui";
            this.radioButtonTraiterOui.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(455, 217);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(92, 13);
            this.label4.TabIndex = 11;
            this.label4.Text = "Type de problème";
            // 
            // comboBoxProbleme
            // 
            this.comboBoxProbleme.FormattingEnabled = true;
            this.comboBoxProbleme.Location = new System.Drawing.Point(445, 233);
            this.comboBoxProbleme.Name = "comboBoxProbleme";
            this.comboBoxProbleme.Size = new System.Drawing.Size(207, 21);
            this.comboBoxProbleme.TabIndex = 10;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(58, 82);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(66, 13);
            this.label1.TabIndex = 13;
            this.label1.Text = "Description :";
            // 
            // textBoxDescription
            // 
            this.textBoxDescription.Location = new System.Drawing.Point(58, 101);
            this.textBoxDescription.Multiline = true;
            this.textBoxDescription.Name = "textBoxDescription";
            this.textBoxDescription.Size = new System.Drawing.Size(200, 135);
            this.textBoxDescription.TabIndex = 12;
            // 
            // buttonRetour
            // 
            this.buttonRetour.Location = new System.Drawing.Point(182, 330);
            this.buttonRetour.Name = "buttonRetour";
            this.buttonRetour.Size = new System.Drawing.Size(75, 23);
            this.buttonRetour.TabIndex = 14;
            this.buttonRetour.Text = "Fermer";
            this.buttonRetour.UseVisualStyleBackColor = true;
            this.buttonRetour.Click += new System.EventHandler(this.buttonRetour_Click);
            // 
            // buttonValider
            // 
            this.buttonValider.Location = new System.Drawing.Point(445, 330);
            this.buttonValider.Name = "buttonValider";
            this.buttonValider.Size = new System.Drawing.Size(75, 23);
            this.buttonValider.TabIndex = 15;
            this.buttonValider.Text = "Valider";
            this.buttonValider.UseVisualStyleBackColor = true;
            this.buttonValider.Click += new System.EventHandler(this.buttonValider_Click);
            // 
            // buttonSupprimer
            // 
            this.buttonSupprimer.Location = new System.Drawing.Point(315, 330);
            this.buttonSupprimer.Name = "buttonSupprimer";
            this.buttonSupprimer.Size = new System.Drawing.Size(75, 23);
            this.buttonSupprimer.TabIndex = 16;
            this.buttonSupprimer.Text = "Supprimer";
            this.buttonSupprimer.UseVisualStyleBackColor = true;
            this.buttonSupprimer.Click += new System.EventHandler(this.buttonSupprimer_Click);
            // 
            // DetailDeclaration
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.buttonSupprimer);
            this.Controls.Add(this.buttonValider);
            this.Controls.Add(this.buttonRetour);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBoxDescription);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.comboBoxProbleme);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "DetailDeclaration";
            this.Text = "Detail";
            this.Load += new System.EventHandler(this.DetailDeclaration_Load);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton radioButtonUrgenceNon;
        private System.Windows.Forms.RadioButton radioButtonUrgenceOui;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton radioButtonTraiterNon;
        private System.Windows.Forms.RadioButton radioButtonTraiterOui;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox comboBoxProbleme;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxDescription;
        private System.Windows.Forms.Button buttonRetour;
        private System.Windows.Forms.Button buttonValider;
        private System.Windows.Forms.Button buttonSupprimer;
    }
}