﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EPP3_Tholdi.Forms
{
    public partial class SelectionDeclaration : Form
    {
        public SelectionDeclaration()
        {
            InitializeComponent();
        }
        /// <summary>
        /// Redirection sur les pages Form en fonction du boutton cliqué
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void boutonFormDeclaration_Click(object sender, EventArgs e)
        {
            AjoutDeclarationForm ajoutDeclaration = new AjoutDeclarationForm();
            ajoutDeclaration.Show();
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            ConsultationDeclarationForm consulterDeclaration = new ConsultationDeclarationForm();
            consulterDeclaration.Show();
            this.Close();
        }

        private void buttonFermerAppli_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
