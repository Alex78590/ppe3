﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PPE3_Tholdi.Classes;

namespace EPP3_Tholdi.Forms
{
    partial class DetailDeclaration : Form
    {
        private Declaration laDeclaration;

        /// <summary>
        /// Initialise un objet Form à partir de la déclaration cliqué précédement 
        /// </summary>
        /// <param name="uneDeclaration"></param>
        public DetailDeclaration(Declaration uneDeclaration)
        {
            laDeclaration = uneDeclaration;
            InitializeComponent();
        }

        /// <summary>
        /// Rempli les controlleurs avec les champs valorisés
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        
        private void DetailDeclaration_Load(object sender, EventArgs e)
        {
            textBoxDescription.Text = laDeclaration.CommentaireDeclaration;
            comboBoxProbleme.Text = laDeclaration.CodeProbleme;
            comboBoxProbleme.SelectedItem = laDeclaration.CodeProbleme;
            comboBoxProbleme.Items.Add("Tag");
            comboBoxProbleme.Items.Add("Corrosion");
            comboBoxProbleme.Items.Add("Choc sur container");
            comboBoxProbleme.Items.Add("Paroi percée");
            comboBoxProbleme.Items.Add("Problème de fermeture");
            comboBoxProbleme.Items.Add("Autre");
            if(laDeclaration.Traite.ToString() == "True")
            {
                radioButtonTraiterOui.Checked = true;
            }
            else
            {
                radioButtonTraiterNon.Checked = true;
            }
            if (laDeclaration.Urgence.ToString() == "True")
            {
                radioButtonUrgenceOui.Checked = true;
            }
            else
            {
                radioButtonUrgenceNon.Checked = true;
            }

        }
        private void buttonSupprimer_Click(object sender, EventArgs e)
        {
            laDeclaration.Supprimer();
            this.Close();
        }

        private void buttonValider_Click(object sender, EventArgs e)
        {
            string description, probleme;
            bool traite = false;
            bool urgent = false;
            description = textBoxDescription.Text;
            probleme = comboBoxProbleme.Text;
            if (radioButtonTraiterOui.Checked)
            {
                traite = true;
            }
            if (radioButtonUrgenceOui.Checked)
            {
                urgent = true;
            }
            laDeclaration.CommentaireDeclaration = description;
            laDeclaration.CodeProbleme = probleme;
            laDeclaration.Traite = traite;
            laDeclaration.Urgence = urgent;
            laDeclaration.Modifier();
            this.Close();
        }

        private void buttonRetour_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
