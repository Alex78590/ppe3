﻿namespace EPP3_Tholdi.Forms
{
    partial class ConsultationDeclarationForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.comboBoxTypeContainer = new System.Windows.Forms.ComboBox();
            this.typeContainer = new System.Windows.Forms.Label();
            this.retour = new System.Windows.Forms.Button();
            this.dataGridViewConsultation = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewConsultation)).BeginInit();
            this.SuspendLayout();
            // 
            // comboBoxTypeContainer
            // 
            this.comboBoxTypeContainer.FormattingEnabled = true;
            this.comboBoxTypeContainer.Location = new System.Drawing.Point(36, 52);
            this.comboBoxTypeContainer.Name = "comboBoxTypeContainer";
            this.comboBoxTypeContainer.Size = new System.Drawing.Size(121, 21);
            this.comboBoxTypeContainer.TabIndex = 2;
            this.comboBoxTypeContainer.SelectedIndexChanged += new System.EventHandler(this.comboBoxTypeContainer_SelectedIndexChanged);
            // 
            // typeContainer
            // 
            this.typeContainer.AutoSize = true;
            this.typeContainer.Location = new System.Drawing.Point(33, 36);
            this.typeContainer.Name = "typeContainer";
            this.typeContainer.Size = new System.Drawing.Size(93, 13);
            this.typeContainer.TabIndex = 5;
            this.typeContainer.Text = "Type de container";
            // 
            // retour
            // 
            this.retour.Location = new System.Drawing.Point(36, 382);
            this.retour.Name = "retour";
            this.retour.Size = new System.Drawing.Size(75, 23);
            this.retour.TabIndex = 6;
            this.retour.Text = "Retour";
            this.retour.UseVisualStyleBackColor = true;
            this.retour.Click += new System.EventHandler(this.retour_Click);
            // 
            // dataGridViewConsultation
            // 
            this.dataGridViewConsultation.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewConsultation.Location = new System.Drawing.Point(36, 106);
            this.dataGridViewConsultation.Name = "dataGridViewConsultation";
            this.dataGridViewConsultation.Size = new System.Drawing.Size(725, 270);
            this.dataGridViewConsultation.TabIndex = 9;
            this.dataGridViewConsultation.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewConsultation_CellClick);
            // 
            // ConsultationDeclarationForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.dataGridViewConsultation);
            this.Controls.Add(this.retour);
            this.Controls.Add(this.typeContainer);
            this.Controls.Add(this.comboBoxTypeContainer);
            this.Name = "ConsultationDeclarationForm";
            this.Text = "ConsultationDeclarationForm";
            this.Load += new System.EventHandler(this.ConsultationDeclarationForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewConsultation)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ComboBox comboBoxTypeContainer;
        private System.Windows.Forms.Label typeContainer;
        private System.Windows.Forms.Button retour;
        private System.Windows.Forms.DataGridView dataGridViewConsultation;
    }
}