﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.DirectoryServices;

namespace EPP3_Tholdi.Forms
{
    public partial class AuthentificationForm : Form
    {
        public AuthentificationForm()
        {
            InitializeComponent();
        }
        /// <summary>
        /// authentification au serveur LDAP des SISR
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonValider_Click(object sender, EventArgs e)
        {
            try

            {
                DirectoryEntry Ldap = new DirectoryEntry("LDAP://tholdi.com/ OU=OU-,OU=OU-,DC=Tholdi,DC=com", "@" + textBoxidentifiant.Text, textBoxMotDePasse.Text);
                DirectoryEntry de = Ldap.NativeObject as DirectoryEntry;
            }



            catch (Exception Ex)

            {


                MessageBox.Show("erreur LDAP " + Ex.Message);

            }
            SelectionDeclaration declaration = new SelectionDeclaration();
            declaration.Show();
            this.Hide();
        }

        private void boutonAnnuler_click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
