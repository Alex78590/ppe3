﻿namespace EPP3_Tholdi.Forms
{
    partial class AjoutDeclarationForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBoxDescription = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.radioButtonTraiterOui = new System.Windows.Forms.RadioButton();
            this.radioButtonTraiterNon = new System.Windows.Forms.RadioButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.radioButtonUrgenceNon = new System.Windows.Forms.RadioButton();
            this.radioButtonUrgenceOui = new System.Windows.Forms.RadioButton();
            this.comboBoxProbleme = new System.Windows.Forms.ComboBox();
            this.groupBoxContainer = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.comboBoxnumeroContainer = new System.Windows.Forms.ComboBox();
            this.comboBoxTypeContainerAjout = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.buttonAnnulerAjoutDeclaration = new System.Windows.Forms.Button();
            this.buttonValiderAjoutDeclaration = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBoxContainer.SuspendLayout();
            this.SuspendLayout();
            // 
            // textBoxDescription
            // 
            this.textBoxDescription.Location = new System.Drawing.Point(41, 50);
            this.textBoxDescription.Multiline = true;
            this.textBoxDescription.Name = "textBoxDescription";
            this.textBoxDescription.Size = new System.Drawing.Size(200, 135);
            this.textBoxDescription.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(41, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(66, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Description :";
            // 
            // radioButtonTraiterOui
            // 
            this.radioButtonTraiterOui.AutoSize = true;
            this.radioButtonTraiterOui.Location = new System.Drawing.Point(13, 27);
            this.radioButtonTraiterOui.Name = "radioButtonTraiterOui";
            this.radioButtonTraiterOui.Size = new System.Drawing.Size(41, 17);
            this.radioButtonTraiterOui.TabIndex = 3;
            this.radioButtonTraiterOui.TabStop = true;
            this.radioButtonTraiterOui.Text = "Oui";
            this.radioButtonTraiterOui.UseVisualStyleBackColor = true;
            // 
            // radioButtonTraiterNon
            // 
            this.radioButtonTraiterNon.AutoSize = true;
            this.radioButtonTraiterNon.Location = new System.Drawing.Point(13, 63);
            this.radioButtonTraiterNon.Name = "radioButtonTraiterNon";
            this.radioButtonTraiterNon.Size = new System.Drawing.Size(45, 17);
            this.radioButtonTraiterNon.TabIndex = 4;
            this.radioButtonTraiterNon.TabStop = true;
            this.radioButtonTraiterNon.Text = "Non";
            this.radioButtonTraiterNon.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.radioButtonTraiterNon);
            this.groupBox1.Controls.Add(this.radioButtonTraiterOui);
            this.groupBox1.Location = new System.Drawing.Point(311, 50);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(104, 100);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Situation traitée";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.radioButtonUrgenceNon);
            this.groupBox2.Controls.Add(this.radioButtonUrgenceOui);
            this.groupBox2.Location = new System.Drawing.Point(460, 50);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(112, 100);
            this.groupBox2.TabIndex = 6;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Situation urgente";
            // 
            // radioButtonUrgenceNon
            // 
            this.radioButtonUrgenceNon.AutoSize = true;
            this.radioButtonUrgenceNon.Location = new System.Drawing.Point(13, 63);
            this.radioButtonUrgenceNon.Name = "radioButtonUrgenceNon";
            this.radioButtonUrgenceNon.Size = new System.Drawing.Size(45, 17);
            this.radioButtonUrgenceNon.TabIndex = 1;
            this.radioButtonUrgenceNon.TabStop = true;
            this.radioButtonUrgenceNon.Text = "Non";
            this.radioButtonUrgenceNon.UseVisualStyleBackColor = true;
            // 
            // radioButtonUrgenceOui
            // 
            this.radioButtonUrgenceOui.AutoSize = true;
            this.radioButtonUrgenceOui.Location = new System.Drawing.Point(13, 27);
            this.radioButtonUrgenceOui.Name = "radioButtonUrgenceOui";
            this.radioButtonUrgenceOui.Size = new System.Drawing.Size(41, 17);
            this.radioButtonUrgenceOui.TabIndex = 0;
            this.radioButtonUrgenceOui.TabStop = true;
            this.radioButtonUrgenceOui.Text = "Oui";
            this.radioButtonUrgenceOui.UseVisualStyleBackColor = true;
            // 
            // comboBoxProbleme
            // 
            this.comboBoxProbleme.FormattingEnabled = true;
            this.comboBoxProbleme.Location = new System.Drawing.Point(311, 222);
            this.comboBoxProbleme.Name = "comboBoxProbleme";
            this.comboBoxProbleme.Size = new System.Drawing.Size(207, 21);
            this.comboBoxProbleme.TabIndex = 7;
            // 
            // groupBoxContainer
            // 
            this.groupBoxContainer.Controls.Add(this.label3);
            this.groupBoxContainer.Controls.Add(this.label2);
            this.groupBoxContainer.Controls.Add(this.comboBoxnumeroContainer);
            this.groupBoxContainer.Controls.Add(this.comboBoxTypeContainerAjout);
            this.groupBoxContainer.Location = new System.Drawing.Point(44, 206);
            this.groupBoxContainer.Name = "groupBoxContainer";
            this.groupBoxContainer.Size = new System.Drawing.Size(197, 169);
            this.groupBoxContainer.TabIndex = 8;
            this.groupBoxContainer.TabStop = false;
            this.groupBoxContainer.Text = "Choix du container";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(14, 84);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(106, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Numéro du container";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(11, 24);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(93, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Type de container";
            // 
            // comboBoxnumeroContainer
            // 
            this.comboBoxnumeroContainer.FormattingEnabled = true;
            this.comboBoxnumeroContainer.Location = new System.Drawing.Point(13, 103);
            this.comboBoxnumeroContainer.Name = "comboBoxnumeroContainer";
            this.comboBoxnumeroContainer.Size = new System.Drawing.Size(174, 21);
            this.comboBoxnumeroContainer.TabIndex = 1;
            // 
            // comboBoxTypeContainerAjout
            // 
            this.comboBoxTypeContainerAjout.FormattingEnabled = true;
            this.comboBoxTypeContainerAjout.Location = new System.Drawing.Point(13, 40);
            this.comboBoxTypeContainerAjout.Name = "comboBoxTypeContainerAjout";
            this.comboBoxTypeContainerAjout.Size = new System.Drawing.Size(174, 21);
            this.comboBoxTypeContainerAjout.TabIndex = 0;
            this.comboBoxTypeContainerAjout.SelectedIndexChanged += new System.EventHandler(this.comboBoxTypeContainerAjout_SelectedIndexChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(321, 206);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(92, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "Type de problème";
            // 
            // buttonAnnulerAjoutDeclaration
            // 
            this.buttonAnnulerAjoutDeclaration.Location = new System.Drawing.Point(311, 274);
            this.buttonAnnulerAjoutDeclaration.Name = "buttonAnnulerAjoutDeclaration";
            this.buttonAnnulerAjoutDeclaration.Size = new System.Drawing.Size(75, 23);
            this.buttonAnnulerAjoutDeclaration.TabIndex = 10;
            this.buttonAnnulerAjoutDeclaration.Text = "Annuler";
            this.buttonAnnulerAjoutDeclaration.UseVisualStyleBackColor = true;
            this.buttonAnnulerAjoutDeclaration.Click += new System.EventHandler(this.buttonAnnulerAjoutDeclaration_Click);
            // 
            // buttonValiderAjoutDeclaration
            // 
            this.buttonValiderAjoutDeclaration.Location = new System.Drawing.Point(442, 274);
            this.buttonValiderAjoutDeclaration.Name = "buttonValiderAjoutDeclaration";
            this.buttonValiderAjoutDeclaration.Size = new System.Drawing.Size(75, 23);
            this.buttonValiderAjoutDeclaration.TabIndex = 11;
            this.buttonValiderAjoutDeclaration.Text = "Valider";
            this.buttonValiderAjoutDeclaration.UseVisualStyleBackColor = true;
            this.buttonValiderAjoutDeclaration.Click += new System.EventHandler(this.buttonValiderAjoutDeclaration_Click);
            // 
            // AjoutDeclarationForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.buttonValiderAjoutDeclaration);
            this.Controls.Add(this.buttonAnnulerAjoutDeclaration);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.groupBoxContainer);
            this.Controls.Add(this.comboBoxProbleme);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBoxDescription);
            this.Name = "AjoutDeclarationForm";
            this.Text = "AjoutDeclarationForm";
            this.Load += new System.EventHandler(this.AjoutDeclarationForm_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBoxContainer.ResumeLayout(false);
            this.groupBoxContainer.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxDescription;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RadioButton radioButtonTraiterOui;
        private System.Windows.Forms.RadioButton radioButtonTraiterNon;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton radioButtonUrgenceNon;
        private System.Windows.Forms.RadioButton radioButtonUrgenceOui;
        private System.Windows.Forms.ComboBox comboBoxProbleme;
        private System.Windows.Forms.GroupBox groupBoxContainer;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox comboBoxnumeroContainer;
        private System.Windows.Forms.ComboBox comboBoxTypeContainerAjout;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button buttonAnnulerAjoutDeclaration;
        private System.Windows.Forms.Button buttonValiderAjoutDeclaration;
    }
}