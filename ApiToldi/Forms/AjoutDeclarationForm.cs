﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PPE3_Tholdi.Classes;

namespace EPP3_Tholdi.Forms
{
    public partial class AjoutDeclarationForm : Form
    {
        public AjoutDeclarationForm()
        {
            InitializeComponent();
        }

        private void buttonValiderAjoutDeclaration_Click(object sender, EventArgs e)
        {
            ajoutDeclaration();
            ConsultationDeclarationForm declaration = new ConsultationDeclarationForm();
            declaration.Show();
            this.Close();
        }

        private void buttonAnnulerAjoutDeclaration_Click(object sender, EventArgs e)
        {
            SelectionDeclaration declaration = new SelectionDeclaration();
            declaration.Show();
            this.Close();
        }
        /// <summary>
        /// Récupère les données des controlleurs pour initialiser un objet de type déclaration
        /// </summary>
        private void ajoutDeclaration()
        {
            int numeroContainer = 0;
            string description, probleme;
            bool traite = false;
            bool urgent = false;
            numeroContainer = Convert.ToInt32(comboBoxnumeroContainer.SelectedItem.ToString());
            description = textBoxDescription.Text;
            probleme = comboBoxProbleme.SelectedItem.ToString();
            if (radioButtonTraiterOui.Checked)
            {
                traite = true;
            }
            if (radioButtonUrgenceOui.Checked)
            {
                urgent = true;
            }

            Declaration laDeclaration = new Declaration()
            {
                NumContainer = numeroContainer,
                CodeProbleme = probleme,
                CommentaireDeclaration = description,
                DateDeclaration = DateTime.Today,
                Urgence = urgent,
                Traite = traite
            };
            laDeclaration.Insert();
        }

        private void AjoutDeclarationForm_Load(object sender, EventArgs e)
        {
            List<string> typeContainer = Containers.recuperationTypeContainer();
            foreach (string untypeContainer in typeContainer)
            {
                comboBoxTypeContainerAjout.Items.Add(untypeContainer);
            }

            comboBoxProbleme.Items.Add("Tag");
            comboBoxProbleme.Items.Add("Corrosion");
            comboBoxProbleme.Items.Add("Choc sur container");
            comboBoxProbleme.Items.Add("Paroi percée");
            comboBoxProbleme.Items.Add("Problème de fermeture");
            comboBoxProbleme.Items.Add("Autre");
        }
        /// <summary>
        /// Modifie les données récupérées sur le numéro de container en fonction du type de container séléctionné
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void comboBoxTypeContainerAjout_SelectedIndexChanged(object sender, EventArgs e)
        {
            string typeContainerRecup = comboBoxTypeContainerAjout.SelectedItem.ToString();
            List<int> numeroContainer = Containers.RecuperationNumeroContainer(typeContainerRecup);
            comboBoxnumeroContainer.Items.Clear();
            foreach(int leNumeroContainer in numeroContainer)
            {
                comboBoxnumeroContainer.Items.Add(leNumeroContainer);
            }
        }
    }
}
