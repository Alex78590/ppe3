﻿namespace EPP3_Tholdi.Forms
{
    partial class AuthentificationForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AuthentificationForm));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.identifiant = new System.Windows.Forms.Label();
            this.textBoxidentifiant = new System.Windows.Forms.TextBox();
            this.textBoxMotDePasse = new System.Windows.Forms.TextBox();
            this.label_motDePasse = new System.Windows.Forms.Label();
            this.buttonValider = new System.Windows.Forms.Button();
            this.boutonAnnuler = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(209, 13);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(331, 103);
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // identifiant
            // 
            this.identifiant.AutoSize = true;
            this.identifiant.Location = new System.Drawing.Point(338, 119);
            this.identifiant.Name = "identifiant";
            this.identifiant.Size = new System.Drawing.Size(53, 13);
            this.identifiant.TabIndex = 0;
            this.identifiant.Text = "Identifiant";
            // 
            // textBoxidentifiant
            // 
            this.textBoxidentifiant.Location = new System.Drawing.Point(277, 135);
            this.textBoxidentifiant.Name = "textBoxidentifiant";
            this.textBoxidentifiant.Size = new System.Drawing.Size(185, 20);
            this.textBoxidentifiant.TabIndex = 2;
            // 
            // textBoxMotDePasse
            // 
            this.textBoxMotDePasse.Location = new System.Drawing.Point(277, 207);
            this.textBoxMotDePasse.Name = "textBoxMotDePasse";
            this.textBoxMotDePasse.Size = new System.Drawing.Size(185, 20);
            this.textBoxMotDePasse.TabIndex = 3;
            // 
            // label_motDePasse
            // 
            this.label_motDePasse.AutoSize = true;
            this.label_motDePasse.Location = new System.Drawing.Point(336, 191);
            this.label_motDePasse.Name = "label_motDePasse";
            this.label_motDePasse.Size = new System.Drawing.Size(71, 13);
            this.label_motDePasse.TabIndex = 4;
            this.label_motDePasse.Text = "Mot de passe";
            // 
            // buttonValider
            // 
            this.buttonValider.Location = new System.Drawing.Point(387, 253);
            this.buttonValider.Name = "buttonValider";
            this.buttonValider.Size = new System.Drawing.Size(75, 23);
            this.buttonValider.TabIndex = 5;
            this.buttonValider.Text = "Valider";
            this.buttonValider.UseVisualStyleBackColor = true;
            this.buttonValider.Click += new System.EventHandler(this.buttonValider_Click);
            // 
            // boutonAnnuler
            // 
            this.boutonAnnuler.Location = new System.Drawing.Point(277, 253);
            this.boutonAnnuler.Name = "boutonAnnuler";
            this.boutonAnnuler.Size = new System.Drawing.Size(75, 23);
            this.boutonAnnuler.TabIndex = 6;
            this.boutonAnnuler.Text = "Annuler";
            this.boutonAnnuler.UseVisualStyleBackColor = true;
            this.boutonAnnuler.Click += new System.EventHandler(this.boutonAnnuler_click);
            // 
            // AuthentificationForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.boutonAnnuler);
            this.Controls.Add(this.buttonValider);
            this.Controls.Add(this.label_motDePasse);
            this.Controls.Add(this.textBoxMotDePasse);
            this.Controls.Add(this.identifiant);
            this.Controls.Add(this.textBoxidentifiant);
            this.Controls.Add(this.pictureBox1);
            this.Name = "AuthentificationForm";
            this.Text = "authentificationForm";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label identifiant;
        private System.Windows.Forms.TextBox textBoxidentifiant;
        private System.Windows.Forms.TextBox textBoxMotDePasse;
        private System.Windows.Forms.Label label_motDePasse;
        private System.Windows.Forms.Button buttonValider;
        private System.Windows.Forms.Button boutonAnnuler;
    }
}