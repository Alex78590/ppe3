﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PPE3_Tholdi.Classes;
using MySql.Data.MySqlClient;

namespace EPP3_Tholdi.Forms
{
    public partial class ConsultationDeclarationForm : Form
    {
        public ConsultationDeclarationForm()
        {
            InitializeComponent();
        }
        private void retour_Click(object sender, EventArgs e)
        {
            SelectionDeclaration retourselection = new SelectionDeclaration();
            retourselection.Show();
            this.Close();
        }

        private void ConsultationDeclarationForm_Load(object sender, EventArgs e)
        {
            List<string> typeContainer = Containers.recuperationTypeContainer();
            foreach (string untypeContainer in typeContainer)
            {
                comboBoxTypeContainer.Items.Add(untypeContainer);
            }
        }

        /// <summary>
        /// Affiche les listes Déclaration par rapport à un type de container
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void comboBoxTypeContainer_SelectedIndexChanged(object sender, EventArgs e)
        {
            dataGridViewConsultation.DataSource = Declaration.FetchAllByContainer(comboBoxTypeContainer.SelectedItem.ToString());
        }

        /// <summary>
        /// Affiche les informations de la déclaration séléctionnées
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGridViewConsultation_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dataGridViewConsultation.SelectedRows.Count > 0)
            {
                Declaration declaration = dataGridViewConsultation.SelectedRows[0].DataBoundItem as Declaration;
                DetailDeclaration detail = new DetailDeclaration(declaration);
                detail.Show();
            }
        }
    }
}

