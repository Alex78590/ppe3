﻿namespace EPP3_Tholdi.Forms
{
    partial class SelectionDeclaration
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.boutonFormDeclaration = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.buttonFermerAppli = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // boutonFormDeclaration
            // 
            this.boutonFormDeclaration.Location = new System.Drawing.Point(302, 106);
            this.boutonFormDeclaration.Name = "boutonFormDeclaration";
            this.boutonFormDeclaration.Size = new System.Drawing.Size(175, 23);
            this.boutonFormDeclaration.TabIndex = 0;
            this.boutonFormDeclaration.Text = "Ajout d\'une déclaration";
            this.boutonFormDeclaration.UseVisualStyleBackColor = true;
            this.boutonFormDeclaration.Click += new System.EventHandler(this.boutonFormDeclaration_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(302, 147);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(175, 23);
            this.button2.TabIndex = 1;
            this.button2.Text = "Consultation des déclaration";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // buttonFermerAppli
            // 
            this.buttonFermerAppli.Location = new System.Drawing.Point(302, 193);
            this.buttonFermerAppli.Name = "buttonFermerAppli";
            this.buttonFermerAppli.Size = new System.Drawing.Size(175, 23);
            this.buttonFermerAppli.TabIndex = 2;
            this.buttonFermerAppli.Text = "Fermer l\'application";
            this.buttonFermerAppli.UseVisualStyleBackColor = true;
            this.buttonFermerAppli.Click += new System.EventHandler(this.buttonFermerAppli_Click);
            // 
            // SelectionDeclaration
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.buttonFermerAppli);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.boutonFormDeclaration);
            this.Name = "SelectionDeclaration";
            this.Text = "Declaration";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button boutonFormDeclaration;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button buttonFermerAppli;
    }
}