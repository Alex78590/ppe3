﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using PPE3_Tholdi.Classes;

namespace PPE3_Tholdi.Classes
{
    class Declaration
    {
        #region  Modèle Objet

        private long codeDeclaration;
        private long numContainer;
        private string codeProbleme;
        private string commentaireDeclaration;
        private DateTime dateDeclaration;
        private bool urgence;
        private bool traite; 



        public long CodeDeclaration
        {
            get { return codeDeclaration; }

            set { codeDeclaration = value;}
        }

        public long NumContainer
        {
            get{ return numContainer; }
            set{ numContainer = value;}
        }

        public string CodeProbleme
        {
            get{ return codeProbleme; }
            set{ codeProbleme = value;}
        }

        public string CommentaireDeclaration
        {
            get{ return commentaireDeclaration; }
            set{ commentaireDeclaration = value; }
        }

        public DateTime DateDeclaration
        {
            get{ return dateDeclaration; }
            set{ dateDeclaration = value; }
        }

        public bool Urgence
        {
            get{ return urgence; }

            set{ urgence = value; }
        }

        public bool Traite
        {
            get{ return traite; }

            set{ traite = value; }
        }

        public Declaration()
        {
            codeDeclaration = -1;
        }

        public Declaration(long numContainer, string codeProbleme, string commentaireDeclaration, DateTime dateDeclaration, bool urgence, bool traite)
        {
            this.numContainer = numContainer;
            this.CodeProbleme = codeProbleme;
            this.commentaireDeclaration = commentaireDeclaration;
            this.dateDeclaration = dateDeclaration;
            this.urgence = urgence;
            this.traite = traite;
        }


        #endregion


        #region Requètes SQL
        /// <summary>
        /// requetes utilisées pour communiquer avec la base de données
        /// </summary>
        private static string _selectSql =
            "SELECT codeDeclaration, numContainer, probleme , commentaireDeclaration, dateDeclaration, urgence, traite FROM DECLARATION";

        private static string _selectSqlByContainer =
            "SELECT codeDeclaration, d.numContainer, probleme , commentaireDeclaration, dateDeclaration, urgence, traite FROM DECLARATION d, CONTAINER c where typeContainer=?typeContainer AND d.numContainer = c.numContainer";

        private static string _insertSql =
            "INSERT INTO DECLARATION (numContainer, probleme , commentaireDeclaration, dateDeclaration, urgence, traite, codeDocker) VALUES (?numContainer, ?probleme, ?commentaireDeclaration, ?dateDeclaration, ?urgence, ?traite, 'AB')";

        private static string _updateSql =
            "UPDATE DECLARATION SET numContainer=?numContainer, probleme=?probleme, commentaireDeclaration=?commentaireDeclaration , dateDeclaration=?dateDeclaration, urgence=?urgence, traite=?traite WHERE codeDeclaration=?codeDeclaration";

        private static string _getLastInsertId =
            "SELECT codeDeclaration FROM DECLARATION where numContainer=?numContainer";

        private static string _supprimerSql =
            "DELETE FROM DECLARATION where codeDeclaration=?codeDeclaration";


        #endregion


        #region Modèle de données

        /// <summary>
        /// Retourne une collection de toutes les déclarations
        /// </summary>
        /// <returns></returns>
        public static List<Declaration> FetchAll()
        {
            List<Declaration> collectionDeclaration = new List<Declaration>();
            MySqlConnection msc = DataBaseAccess.GetConnexion;
            msc.Open();
            MySqlCommand commandSql = msc.CreateCommand();
            commandSql.CommandText = _selectSql;
            MySqlDataReader jeuEnregistrements = commandSql.ExecuteReader();
            while (jeuEnregistrements.Read())
            {
                Declaration uneDeclaration = new Declaration();
                uneDeclaration.codeDeclaration = Convert.ToInt16(jeuEnregistrements["codeDeclaration"].ToString());
                uneDeclaration.numContainer = Convert.ToInt16(jeuEnregistrements["numContainer"].ToString());
                uneDeclaration.codeProbleme = jeuEnregistrements["codeProbleme"].ToString();
                uneDeclaration.commentaireDeclaration = jeuEnregistrements["commentaireDeclaration"].ToString();
                uneDeclaration.dateDeclaration = Convert.ToDateTime(jeuEnregistrements["dateDeclaration"].ToString());
                uneDeclaration.urgence = Convert.ToBoolean(jeuEnregistrements["urgence"]);
                uneDeclaration.traite = Convert.ToBoolean(jeuEnregistrements["traite"]);
                collectionDeclaration.Add(uneDeclaration);
            }
            msc.Close();
            return collectionDeclaration;
        }



        /// <summary>
        /// Retourne une colection de déclaration pour le container mis en parametre
        /// </summary>
        /// <param name="typecontainer"></param>
        /// <returns></returns>
        public static List<Declaration> FetchAllByContainer(string typecontainer)
        {
            List<Declaration> collectionDeclaration = new List<Declaration>();
            MySqlConnection msc = DataBaseAccess.GetConnexion;
            msc.Open();
            MySqlCommand commandSql = msc.CreateCommand();
            commandSql.CommandText = _selectSqlByContainer;
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?typeContainer", typecontainer));
            commandSql.Prepare();
            MySqlDataReader jeuEnregistrements = commandSql.ExecuteReader();
            while (jeuEnregistrements.Read())
            {
                Declaration uneDeclaration = new Declaration();
                uneDeclaration.codeDeclaration = Convert.ToInt16(jeuEnregistrements["codeDeclaration"].ToString());
                uneDeclaration.numContainer = Convert.ToInt16(jeuEnregistrements["numContainer"].ToString());
                uneDeclaration.codeProbleme = jeuEnregistrements["probleme"].ToString();
                uneDeclaration.commentaireDeclaration = jeuEnregistrements["commentaireDeclaration"].ToString();
                uneDeclaration.dateDeclaration = Convert.ToDateTime(jeuEnregistrements["dateDeclaration"].ToString());
                uneDeclaration.urgence = Convert.ToBoolean(jeuEnregistrements["urgence"]);
                uneDeclaration.traite = Convert.ToBoolean(jeuEnregistrements["traite"]);
                collectionDeclaration.Add(uneDeclaration);
            }
            msc.Close();
            return collectionDeclaration;
        }

        /// <summary>
        /// Supprimer une déclaration à l'aide de son codeDéclaration
        /// </summary>
        public void Supprimer()
        {
            MySqlConnection msc = DataBaseAccess.GetConnexion;
            msc.Open();
            MySqlCommand commandSql = msc.CreateCommand();
            commandSql.CommandText = _supprimerSql;
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?codeDeclaration", codeDeclaration));
            commandSql.Prepare();
            int nbLignesAffectees = commandSql.ExecuteNonQuery();
            codeDeclaration = commandSql.LastInsertedId;
            msc.Close();
        }

        /// <summary>
        /// Insère une Déclaration dans la base de données
        /// </summary>
        public void Insert()
        {
            MySqlConnection msc = DataBaseAccess.GetConnexion;
            msc.Open();
            MySqlCommand commandSql = msc.CreateCommand();
            commandSql.CommandText = _insertSql;
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?numContainer", numContainer));
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?probleme", codeProbleme));
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?commentaireDeclaration", commentaireDeclaration));
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?dateDeclaration", dateDeclaration));
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?urgence", urgence));
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?traite", traite));
            commandSql.Prepare();
            int nbLignesAffectees = commandSql.ExecuteNonQuery();
            codeDeclaration = commandSql.LastInsertedId;
            msc.Close();
        }
        ///<summary>
        ///Modifier une déclaration à l'aide de son codeDeclaration
        ///</summary>
        public void Modifier()
        {
            MySqlConnection msc = DataBaseAccess.GetConnexion;
            msc.Open();
            MySqlCommand commandSql = msc.CreateCommand();
            commandSql.CommandText = _updateSql;
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?numContainer", numContainer));
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?probleme", CodeProbleme));
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?commentaireDeclaration", commentaireDeclaration));
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?dateDeclaration", dateDeclaration));
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?urgence", urgence));
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?traite", traite));
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?codeDeclaration", codeDeclaration));
            commandSql.Prepare();
            int nbLignesAffectees = commandSql.ExecuteNonQuery();
            codeDeclaration = commandSql.LastInsertedId;
            msc.Close();
        }

        #endregion

    }
}

