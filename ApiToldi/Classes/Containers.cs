﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using PPE3_Tholdi.Classes;

namespace PPE3_Tholdi.Classes
{
    public class Containers
    {
        #region CONSTRUCTEUR
        private long numContainer;
        private DateTime dateAchat;
        private string typeContainer;
        private DateTime dateDerniereInsp;
        //private List<Inspection> lesInspections; 

        public long NumContainer
        {
            get { return numContainer; }
            set { numContainer = value; }
        }

        public string TypeContainer
        {
            get { return typeContainer; }
            set { typeContainer = value; }
        }

        public DateTime DateAchat
        {
            get { return dateAchat; }
            set { dateAchat = value; }
        }

        public DateTime DateDerniereInsp
        {
            get { return dateDerniereInsp; }
            set { dateDerniereInsp = value; }
        }

        public Containers()
        {
            numContainer = -1;
        }
        #endregion

        #region Requete SQL
        /// <summary>
        /// requetes permettants de communiquer avec la base de données
        /// </summary>
        private static string _selectSql =
            "SELECT numContainer, dateAchat, typeContainer, dateDerniereInsp FROM CONTAINER";

        private static string _recupTypeContainer =
            "select distinct typeContainer FROM CONTAINER";

        private static string _selectSqlAvecTypeContainer =
            "SELECT numContainer FROM CONTAINER where typeContainer = ?typeContainer";

        #endregion

        #region FetchAll
        public static List<Containers> FetchAll()
        {
            List<Containers> resultat = new List<Containers>();
            MySqlConnection msc = DataBaseAccess.GetConnexion;
            msc.Open();
            MySqlCommand commandSql = msc.CreateCommand();
            commandSql.CommandText = _selectSql;
            MySqlDataReader jeuEnregistrements = commandSql.ExecuteReader();
            while (jeuEnregistrements.Read())
            {
                Containers unContainer = new Containers();

                string numContainer = jeuEnregistrements["numContainer"].ToString();
                unContainer.numContainer = Convert.ToInt16(numContainer);
                unContainer.typeContainer = jeuEnregistrements["typeContainer"].ToString();
                unContainer.DateAchat = Convert.ToDateTime(jeuEnregistrements["dateAchat"].ToString());
                unContainer.dateDerniereInsp = Convert.ToDateTime(jeuEnregistrements["dateDerniereInsp"]);
                resultat.Add(unContainer);
            }
            msc.Close();
            return resultat;
        }

        /// <summary>
        /// Renvoi la liste des type de containers
        /// </summary>
        /// <returns></returns>
        public static List<string> recuperationTypeContainer()
        {
            List<string> typeContainer = new List<string>();
            MySqlConnection msc = DataBaseAccess.GetConnexion;
            msc.Open();
            MySqlCommand commandSql = msc.CreateCommand();
            commandSql.CommandText = _recupTypeContainer;
            MySqlDataReader jeuEnregistrements = commandSql.ExecuteReader();
            while (jeuEnregistrements.Read())
            { 
                typeContainer.Add(jeuEnregistrements["typeContainer"].ToString());
            }
            msc.Close();
            return typeContainer;
        }

        /// <summary>
        /// Renvoi la liste de numéro de containers pour un type de container donné
        /// </summary>
        /// <param name="typeContainer"></param>
        /// <returns></returns>
        public static List<int> RecuperationNumeroContainer(string typeContainer)
        {
            List<int> numeroContainer = new List<int>();
            MySqlConnection msc = DataBaseAccess.GetConnexion;
            msc.Open();
            MySqlCommand commandSql = msc.CreateCommand();
            commandSql.CommandText = _selectSqlAvecTypeContainer;
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?typeContainer", typeContainer));
            commandSql.Prepare();
            MySqlDataReader jeuEnregistrements = commandSql.ExecuteReader();
            while(jeuEnregistrements.Read()){
                numeroContainer.Add(Convert.ToInt32(jeuEnregistrements["numContainer"]));
            }
            return numeroContainer;
        }

        #endregion
    }
}

